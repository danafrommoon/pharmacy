package services.interfaces;

import domain.Medicine;
public interface IMedicineService {
    Medicine medicineInfo(String type, String medicine);
    void addMedicine(Medicine medicine);
}
