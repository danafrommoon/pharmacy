package repositories.interfaces;

import domain.Medicine;

public interface IMedicineRepository {
    String medicineInfo (String type, String medicinename);
    void addMedicine(Medicine medicine);
}
