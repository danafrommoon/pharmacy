package repositories.entities;

import domain.Medicine;
import repositories.MySQLRepository;
import repositories.interfaces.IMedicineRepository;

import javax.ws.rs.BadRequestException;
import java.sql.*;

public class MedicineRepository implements IMedicineRepository{
    private Connection mysqlConn;

    public MedicineRepository(){
        mysqlConn = MySQLRepository.getConnection();
    }
    @Override
    public String medicineInfo(String type, String medicinename) {
        try{
String sql = "SELECT * FROM meds ";
PreparedStatement stmt = mysqlConn.prepareStatement(sql);
stmt.setString(1, type);
stmt.setString(2,medicinename);
ResultSet rs = stmt.executeQuery();
        }catch (SQLException e){
            throw new BadRequestException();
        }
        return medicinename;
    }

    @Override
    public void addMedicine(Medicine medicine) {
try{
    String sql= "INSERT INTO medicines VALUES( ? )";
    PreparedStatement stmt = mysqlConn.prepareStatement(sql);
    stmt.setString(1,medicine.getName());
    stmt.execute();
}catch (SQLException e){
    throw new BadRequestException();
}
    }
}
