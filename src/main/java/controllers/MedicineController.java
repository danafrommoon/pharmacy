package controllers;

import domain.Medicine;
import services.MedicineService;
import services.interfaces.IMedicineService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/medicine")
public class MedicineController {
    private IMedicineService medicineService;
    private String meds;

    public MedicineController(){
        medicineService = new MedicineService();
    }

    @GET
    @Path("/{type}/medicine")
    public Response getInfo(@PathParam("type")String type, @PathParam("type")String meds){
        this.meds = meds;
        Medicine medicine;
        try {
            medicine = medicineService.medicineInfo(type, meds);
        } catch (ServerErrorException e){
            return Response
                    .serverError()
                    .build();
        } catch (BadRequestException e){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
        if(medicine == null) {return Response
        .status(Response.Status.NOT_FOUND)
        .build();} else {
            return Response
                    .ok(medicine)
                    .build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response addMedicine(Medicine medicine){
        try{
            medicineService.addMedicine(medicine);
        } catch (ServerErrorException e){
            return Response
                    .serverError()
                    .build();
        } catch (BadRequestException e){
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .build();
        }
        return Response
                .status(Response.Status. CREATED)
                .entity("Medicine added successfully!")
                .build();
    }
}
