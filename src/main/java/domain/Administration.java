package domain;

public class Administration implements interfaces.Administration {
    @Override
    public void ControlWork(String sphere) {
        System.out.println("Admin cotrolled "+ sphere);
    }

    @Override
    public void CheckMedicines(String name, int amount_of_med) {
        System.out.println("There have "+amount_of_med + " " +name);
    }
}
