package domain;

public enum Role {
    admin,
    pharmacist,
    client
}
