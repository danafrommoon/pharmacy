package domain;

public class Medicine {
    private int id;
    private String name;
    private String type;

    public Medicine(){

    }

    public Medicine (int id, String name){
        setId(id);
        setName(name);
    }

    public Medicine(int id, String name, String type){
        setId(id);
        setName(name);
        setType(type);
    }

    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
