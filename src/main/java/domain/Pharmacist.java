package domain;

public class Pharmacist implements interfaces.Pharmacist {
    @Override
    public void GivingMedicine(String name, int price_of_med) {
        System.out.println("Pharmacist sold "+ name + "with price "+ price_of_med);
    }
}
